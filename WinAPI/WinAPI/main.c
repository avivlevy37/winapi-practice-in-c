#include <windows.h>
#define WINDOW_CLASS_NAME (TEXT("WinAPI Summary Window"))

#if defined(UNICODE)
#define ENTER (13)
#else
#define ENTER ('\n')
#endif

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nShowCmd)
{
	WNDCLASS wc = { 0 };
	HWND hWnd = NULL;
	MSG msg = { 0 };
	/* Fill the Window Class structure*/
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.lpszClassName = WINDOW_CLASS_NAME;
	wc.lpszMenuName = NULL;
	wc.hInstance = hInstance;
	/* Register the class (also check if the function failed) */
	if (FALSE == RegisterClass(&wc))
	{
		MessageBox(NULL, TEXT("Window Class Registration Failed!"), TEXT("Error"),
			MB_OK | MB_ICONWARNING);
		return -1;
	}
	/* Create the window */
	hWnd = CreateWindow(WINDOW_CLASS_NAME, TEXT("Sample Title"),
		WS_OVERLAPPEDWINDOW, 0, 0, 200, 200, NULL, NULL, hInstance, NULL);
	if (NULL == hWnd)
	{
		MessageBox(NULL, TEXT("Window creation failed!"), TEXT("Error"),
			MB_OK | MB_ICONWARNING);
		return -1;
	}
	/* Show the window */
	ShowWindow(hWnd, nShowCmd);
	/* The message loop */
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	/* When GetMessage receives the WM_QUIT message, then wParam will indicate
	the required return code */
	return msg.wParam;
}


#define BUFFER_SIZE (64)
#define PATH_SIZE (MAX_PATH + 1)
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static UINT rightClickCount = 0;
	CHAR buffer[BUFFER_SIZE] = { 0 }, systemDirectory[PATH_SIZE] = { 0 };
	HANDLE hFile = NULL;
	SYSTEMTIME time = { 0 };
	switch (msg)
	{
	case WM_RBUTTONDOWN:
		rightClickCount++;
		return 0;

	case WM_LBUTTONDOWN:
		wsprintfA(buffer, "%u", (UINT)rightClickCount);
		SetWindowTextA(hWnd, buffer);
		return 0;

	case WM_CHAR:
		switch (wParam)
		{
		case '?':
			GetSystemDirectoryA(systemDirectory, PATH_SIZE);
			MessageBoxA(hWnd, systemDirectory, "System Directory", MB_OK);
			return 0;

		case ENTER:
			if (INVALID_HANDLE_VALUE == (hFile = CreateFile(TEXT("time.txt"), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY, NULL)))
			{
				wsprintfA(buffer, "Failed to open file: %ul", GetLastError());
				MessageBox(hWnd, buffer, TEXT("Error"), MB_ICONERROR | MB_OK);
				return -1;
			}

			GetLocalTime(&time);
			GetTimeFormatA(LOCALE_SYSTEM_DEFAULT, LOCALE_USE_CP_ACP, &time, NULL, buffer, BUFFER_SIZE);
			WriteFile(hFile, buffer, strlen(buffer), NULL, NULL);
			CloseHandle(hFile);
			return 0;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}
